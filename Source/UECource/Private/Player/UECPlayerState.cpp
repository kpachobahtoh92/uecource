// Unreal Engine course project


#include "Player/UECPlayerState.h"
DEFINE_LOG_CATEGORY_STATIC(LogUECPlayerState, All, All);

void AUECPlayerState::LogInfo()
{
	UE_LOG(LogUECPlayerState, Display, TEXT("TeamID: %i, Kills: %i, Deaths: %i"), TeamID, KillsNum, DeathsNum);
}
