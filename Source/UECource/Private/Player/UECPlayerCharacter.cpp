// Unreal Engine course project


#include "Player/UECPlayerCharacter.h"

#include "Camera/CameraComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Player/Components/UECWeaponComponent.h"
#include "Components/CapsuleComponent.h"

AUECPlayerCharacter::AUECPlayerCharacter(const FObjectInitializer& ObjInit) : Super(ObjInit)
{
	PrimaryActorTick.bCanEverTick = true;

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>("SpringArmComponent");
	SpringArmComponent->SetupAttachment(GetRootComponent());
	SpringArmComponent->bUsePawnControlRotation = true;
	SpringArmComponent->SocketOffset = FVector(0.f, 100.f, 80.f);

	CameraComponent = CreateDefaultSubobject<UCameraComponent>("CameraComponent");
	CameraComponent->SetupAttachment(SpringArmComponent);

	CameraCollisionComponent = CreateDefaultSubobject<USphereComponent>("CameraCollisionComponent");
	CameraCollisionComponent->SetupAttachment(CameraComponent);
	CameraCollisionComponent->SetSphereRadius(10.f);
	CameraCollisionComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

void AUECPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	check(PlayerInputComponent);
	check(WeaponComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &AUECPlayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AUECPlayerCharacter::MoveRight);
	PlayerInputComponent->BindAxis("LookUp", this, &AUECPlayerCharacter::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("TurnAround", this, &AUECPlayerCharacter::AddControllerYawInput);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AUECPlayerCharacter::Jump);
	PlayerInputComponent->BindAction("Run", IE_Pressed, this, &AUECPlayerCharacter::StartRun);
	PlayerInputComponent->BindAction("Run", IE_Released, this, &AUECPlayerCharacter::EndRun);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, WeaponComponent, &UUECWeaponComponent::StartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, WeaponComponent, &UUECWeaponComponent::StopFire);
	PlayerInputComponent->BindAction("NextWeapon", IE_Pressed, WeaponComponent, &UUECWeaponComponent::NextWeapon);
	PlayerInputComponent->BindAction("Reload", IE_Pressed, WeaponComponent, &UUECWeaponComponent::Reload);
}

void AUECPlayerCharacter::OnDeath()
{
	Super::OnDeath();
	if (Controller)
	{
		Controller->ChangeState(NAME_Spectating);
	}
}

void AUECPlayerCharacter::CheckCameraOverlap()
{
	const auto bIsHideMesh = CameraCollisionComponent->IsOverlappingComponent(GetCapsuleComponent());
	GetMesh()->SetOwnerNoSee(bIsHideMesh);
	TArray<USceneComponent*> MeshChildren;
	GetMesh()->GetChildrenComponents(true, MeshChildren);
	for (auto MeshChild : MeshChildren)
	{
		UPrimitiveComponent* PrimitiveComponent = Cast<UPrimitiveComponent>(MeshChild);
		if (PrimitiveComponent)
		{
			PrimitiveComponent->SetOwnerNoSee(bIsHideMesh);
		}
	}
}

void AUECPlayerCharacter::OnCameraCollisionComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent,
                                                                 AActor* OtherActor, UPrimitiveComponent* OtherComp,
                                                                 int32 OtherBodyIndex, bool bFromSweep,
                                                                 const FHitResult& SweepResult)
{
	CheckCameraOverlap();
}

void AUECPlayerCharacter::OnCameraCollisionComponentEndOverlap(UPrimitiveComponent* OverlappedComponent,
                                                               AActor* OtherActor, UPrimitiveComponent* OtherComp,
                                                               int32 OtherBodyIndex)
{
	CheckCameraOverlap();
}

void AUECPlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	check(CameraCollisionComponent);
	CameraCollisionComponent->OnComponentBeginOverlap.AddDynamic(
		this, &AUECPlayerCharacter::OnCameraCollisionComponentBeginOverlap);
	CameraCollisionComponent->OnComponentEndOverlap.AddDynamic(
		this, &AUECPlayerCharacter::OnCameraCollisionComponentEndOverlap);
}

void AUECPlayerCharacter::MoveForward(float Amount)
{
	AddMovementInput(GetActorForwardVector(), Amount);
}

void AUECPlayerCharacter::MoveRight(float Amount)
{
	AddMovementInput(GetActorRightVector(), Amount);
}

void AUECPlayerCharacter::StartRun()
{
	GetCharacterMovement()->MaxWalkSpeed = 900.f;
}

void AUECPlayerCharacter::EndRun()
{
	GetCharacterMovement()->MaxWalkSpeed = 600.f;
}
