// Unreal Engine cource project.


#include "Player/UECBaseCharacter.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Player/Components/UECCharacterMovementComponent.h"
#include "Player/Components/UECHealthComponent.h"
#include "Player/Components/UECWeaponComponent.h"

DEFINE_LOG_CATEGORY_STATIC(BaseCharacterLog, All, All);

// Sets default values
AUECBaseCharacter::AUECBaseCharacter(const FObjectInitializer& ObjInit)
:Super(ObjInit.SetDefaultSubobjectClass<UUECCharacterMovementComponent>(CharacterMovementComponentName))
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	HealthComponent = CreateDefaultSubobject<UUECHealthComponent>("HealthComponent");
	WeaponComponent = CreateDefaultSubobject<UUECWeaponComponent>("WeaponComponent");
}

void AUECBaseCharacter::OnGroundLanded(const FHitResult& HitResult)
{
	const double FallVelocityZ = -GetCharacterMovement()->Velocity.Z;
	UE_LOG(BaseCharacterLog, Display, TEXT("On landed: %f"), FallVelocityZ);
	if (FallVelocityZ < LandedDamageVelocity.X)
	{
		return;
	}
	const auto FinalDamage = FMath::GetMappedRangeValueClamped(LandedDamageVelocity, LandedDamage, FallVelocityZ);
	TakeDamage(FinalDamage, FDamageEvent{}, nullptr, nullptr);
	UE_LOG(BaseCharacterLog, Display, TEXT("FinalDamage: %f"), FinalDamage);
}

void AUECBaseCharacter::OnDeath()
{
	UE_LOG(LogTemp, Display, TEXT("Player %s is dead"), *GetName());

	//PlayAnimMontage(DeathAnimMontage);
	GetCharacterMovement()->DisableMovement();
	SetLifeSpan(5.f);
	GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECR_Ignore);
	WeaponComponent->StopFire();
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	GetMesh()->SetSimulatePhysics(true);
}

void AUECBaseCharacter::OnHealthChange(float Health, float HealthDelta)
{
	
}

// Called when the game starts or when spawned
void AUECBaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	check(HealthComponent);
	check(GetCharacterMovement());
	check(GetMesh());

	OnHealthChange(HealthComponent->GetHealth(), 0.f);
	HealthComponent->OnDeath.AddUObject(this, &AUECBaseCharacter::OnDeath);
	HealthComponent->OnHealthChange.AddUObject(this, &AUECBaseCharacter::OnHealthChange);
	LandedDelegate.AddDynamic(this, &AUECBaseCharacter::OnGroundLanded);
}

// Called every frame
void AUECBaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

float AUECBaseCharacter::GetMovementDirection() const
{
	if (GetVelocity().IsZero()) return 0.f;
	const auto VelocityNormal = GetVelocity().GetSafeNormal();
	const auto AngleBetween = FMath::Acos(FVector::DotProduct(GetActorForwardVector(), VelocityNormal));
	const auto CrossProduct = FVector::CrossProduct(GetActorForwardVector(), VelocityNormal);
	const auto Degrees = FMath::RadiansToDegrees(AngleBetween);
	return CrossProduct.IsZero() ? Degrees : Degrees * FMath::Sign(CrossProduct.Z);
}

void AUECBaseCharacter::SetPlayerColor(FLinearColor& LinearColor)
{
	const auto MaterialInstance = GetMesh()->CreateAndSetMaterialInstanceDynamic(0);
	if (!MaterialInstance)
	{
		return;
	}
	MaterialInstance->SetVectorParameterValue(MaterialColorName, LinearColor);
}


