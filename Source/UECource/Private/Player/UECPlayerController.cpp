// Unreal Engine course project


#include "Player/UECPlayerController.h"

#include "UECGameModeBase.h"
#include "GameFramework/GameModeBase.h"
#include "Player/Components/UECRespawnComponent.h"
#include "UI/UECGameHUD.h"

AUECPlayerController::AUECPlayerController()
{
	RespawnComponent = CreateDefaultSubobject<UUECRespawnComponent>("RespawnComponent");
}

void AUECPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	if (!InputComponent)
	{
		return;
	}
	InputComponent->BindAction("PauseGame", IE_Pressed, this, &AUECPlayerController::OnPauseGame);
}

void AUECPlayerController::OnMatchStateChange(EUECMatchState MatchState)
{
	if (MatchState == MS_InProgress)
	{
		SetInputMode(FInputModeGameOnly());
		SetShowMouseCursor(false);
	}
	else
	{
		SetInputMode(FInputModeUIOnly());
		SetShowMouseCursor(true);
	}
}

void AUECPlayerController::BeginPlay()
{
	Super::BeginPlay();
	if (GetWorld())
	{
		AUECGameModeBase* GameModeBase = Cast<AUECGameModeBase>(GetWorld()->GetAuthGameMode());
		if (GameModeBase)
		{
			GameModeBase->OnMatchStateChanged.AddUObject(this, &AUECPlayerController::OnMatchStateChange);
		}
	}
}

void AUECPlayerController::OnPauseGame()
{
	if (!GetWorld() || !GetWorld()->GetAuthGameMode())
	{
		return;
	}
	GetWorld()->GetAuthGameMode()->SetPause(this);
}
