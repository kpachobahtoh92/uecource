// Unreal Engine course project


#include "Player/Components/UECHealthComponent.h"

#include "UECGameModeBase.h"
#include "Kismet/BlueprintTypeConversions.h"
#include "Kismet/KismetSystemLibrary.h"


DEFINE_LOG_CATEGORY_STATIC(LogHealthComponent, All, All);

UUECHealthComponent::UUECHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

bool UUECHealthComponent::TryToAddHealth(int32 HealthAmount)
{
	if (IsHealthFull())
	{
		return false;
	}

	SetHealth(GetHealth() + HealthAmount);
	return true;
}

bool UUECHealthComponent::IsHealthFull() const
{
	return FMath::IsNearlyEqual(Health, MaxHealth);
}

void UUECHealthComponent::AutoHeal()
{
	SetHealth(Health + AutoHealHPCount);
	if (IsHealthFull() && GetWorld())
	{
		GetWorld()->GetTimerManager().ClearTimer(AutoHealTimerHandle);
	}
}

void UUECHealthComponent::BeginPlay()
{
	Super::BeginPlay();
	check(MaxHealth>0);
	SetHealth(MaxHealth);

	if (AActor* ComponentOwner = GetOwner())
	{
		ComponentOwner->OnTakeAnyDamage.AddDynamic(this, &UUECHealthComponent::OnTakeAnyDamage);
	}
}

void UUECHealthComponent::OnTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
	AController* InstigatedBy, AActor* DamageCauser)
{
	if (Damage <= 0.f || IsDead() || !GetWorld())
	{
		return;
	}
	SetHealth(Health - Damage);
	if (IsDead())
	{
		Killed(InstigatedBy);
		OnDeath.Broadcast();
		GetWorld()->GetTimerManager().ClearTimer(AutoHealTimerHandle);
	}
	if (bIsAutoHealEnabled && !IsDead())
	{
		GetWorld()->GetTimerManager().ClearTimer(AutoHealTimerHandle);
		GetWorld()->GetTimerManager().SetTimer(AutoHealTimerHandle, this, &UUECHealthComponent::AutoHeal,
		                                       AutoHealFrequency, true, AutoHealDelay);
	}
	PlayCameraShake();
}

void UUECHealthComponent::SetHealth(float NewHealth)
{
	const auto NextHealth = FMath::Clamp(NewHealth, 0.f, MaxHealth);
	const auto HealthDelta = NextHealth - Health;
	Health = NextHealth;
	OnHealthChange.Broadcast(Health, HealthDelta);
}

void UUECHealthComponent::PlayCameraShake()
{
	if (IsDead())
	{
		return;
	}
	const auto Player = Cast<APawn>(GetOwner());
	if (!Player)
	{
		return;
	}
	const auto Controller = Player->GetController<APlayerController>();
	if (!Controller || !Controller->PlayerCameraManager)
	{
		return;
	}
	Controller->PlayerCameraManager->StartCameraShake(CameraShake);
}

void UUECHealthComponent::Killed(AController* KillerController)
{
	const auto GameMode = Cast<AUECGameModeBase>(GetWorld()->GetAuthGameMode());
	if (!GameMode)
	{
		return;
	}
	const auto Player = Cast<APawn>(GetOwner());
	const auto VictimController = Player ? Player->GetController() : nullptr;
	GameMode->Killed(KillerController, VictimController);
	
}
