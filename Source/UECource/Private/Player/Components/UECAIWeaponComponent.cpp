// Unreal Engine course project


#include "Player/Components/UECAIWeaponComponent.h"


// Sets default values for this component's properties
UUECAIWeaponComponent::UUECAIWeaponComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UUECAIWeaponComponent::StartFire()
{
	if (!CanFire())
	{
		return;
	}
	if (CurrentWeapon->IsAmmoEmpty())
	{
		NextWeapon();
	}
	else
	{
		CurrentWeapon->StartFire();
	}
}

void UUECAIWeaponComponent::NextWeapon()
{
	if (!CanEquip())
	{
		return;
	}
	int32 NextIndex = (CurrentWeaponIndex + 1 ) % Weapons.Num();
	while (NextIndex != CurrentWeaponIndex)
	{
		if (!Weapons[NextIndex]->IsAmmoEmpty())
		{
			break;
		}
		NextIndex = (NextIndex +1) % Weapons.Num();
	}
	if (CurrentWeaponIndex != NextIndex)
	{
		CurrentWeaponIndex = NextIndex;
		EquipWeapon(CurrentWeaponIndex);
	}
}


// Called when the game starts
void UUECAIWeaponComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UUECAIWeaponComponent::TickComponent(float DeltaTime, ELevelTick TickType,
                                          FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

