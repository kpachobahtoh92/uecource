// Unreal Engine course project


#include "Player/Components/UECRespawnComponent.h"

#include "UECGameModeBase.h"


UUECRespawnComponent::UUECRespawnComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UUECRespawnComponent::Respawn(int32 RespawnTime)
{
	if (!GetWorld())
	{
		return;
	}
	RespawnCountDown = RespawnTime;
	GetWorld()->GetTimerManager().SetTimer(RespawnTimerHandle, this, &UUECRespawnComponent::RespawnTimerUpdate, 1.f, true);
}

bool UUECRespawnComponent::IsRespawnInProgress()
{
	return GetWorld() && GetWorld()->GetTimerManager().IsTimerActive(RespawnTimerHandle);
}

void UUECRespawnComponent::RespawnTimerUpdate()
{
	if (--RespawnCountDown == 0)
	{
		if (!GetWorld())
		{
			return;
		}
		GetWorld()->GetTimerManager().ClearTimer(RespawnTimerHandle);
		AUECGameModeBase* GameModeBase = Cast<AUECGameModeBase>(GetWorld()->GetAuthGameMode());
		if (!GameModeBase)
		{
			return;
		}

		GameModeBase->RespawnRequest(Cast<AController>(GetOwner()));
	}
}

void UUECRespawnComponent::BeginPlay()
{
	Super::BeginPlay();
}
