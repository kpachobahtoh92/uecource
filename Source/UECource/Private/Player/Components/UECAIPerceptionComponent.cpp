// Unreal Engine course project


#include "Player/Components/UECAIPerceptionComponent.h"

#include "AIController.h"
#include "UECUtils.h"
#include "Perception/AISense_Sight.h"
#include "Player/Components/UECHealthComponent.h"


// Sets default values for this component's properties
UUECAIPerceptionComponent::UUECAIPerceptionComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UUECAIPerceptionComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UUECAIPerceptionComponent::TickComponent(float DeltaTime, ELevelTick TickType,
                                              FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

AActor* UUECAIPerceptionComponent::GetClosestEnemy()
{
	TArray<AActor*> PercieveActors;
	GetCurrentlyPerceivedActors(UAISense_Sight::StaticClass(), PercieveActors);
	if (PercieveActors.Num() == 0)
	{
		return nullptr;
	}
	const auto Controller = Cast<AAIController>(GetOwner());
	if (!Controller)
	{
		return nullptr;
	}
	const auto Pawn = Controller->GetPawn();
	if (!Pawn)
	{
		return nullptr;
	}
	float BestDistance = MAX_FLT;
	AActor* BestPawn = nullptr;
	for (auto PercieveActor : PercieveActors)
	{
		const auto HealthComponent = UECUtils::GetUECPlayerComponent<UUECHealthComponent>(PercieveActor);
		APawn* PercievePawn = Cast<APawn>(PercieveActor);
		bool AreEnemies = PercievePawn && UECUtils::AreEnemies(Controller, PercievePawn->Controller);
		if (HealthComponent && !HealthComponent->IsDead() && AreEnemies)
		{
			const auto CurrentDistance = (PercieveActor->GetActorLocation() - Pawn->GetActorLocation()).Size();
			if (CurrentDistance < BestDistance)
			{
				BestDistance = CurrentDistance;
				BestPawn = PercieveActor;
			}
		}
	}
	return BestPawn;
}

