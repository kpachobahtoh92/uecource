// Unreal Engine course project


#include "PickUps/UECHealthPickup.h"

#include "UECUtils.h"
#include "Player/Components/UECHealthComponent.h"

class UUECHealthComponent;
DEFINE_LOG_CATEGORY_STATIC(LogHealthPickup, All, All);

// Sets default values
AUECHealthPickup::AUECHealthPickup()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AUECHealthPickup::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AUECHealthPickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

bool AUECHealthPickup::GivePickupTo(APawn* PlayerPawn)
{
	const auto HealthComponent = UECUtils::GetUECPlayerComponent<UUECHealthComponent>(PlayerPawn);
	if (!HealthComponent || HealthComponent->IsDead())
	{
		return false;
	}
	UE_LOG(LogHealthPickup, Display, TEXT("Health was taken"));
	return HealthComponent->TryToAddHealth(HealthAmount);
}

