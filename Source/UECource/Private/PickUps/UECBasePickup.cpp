// Unreal Engine course project


#include "PickUps/UECBasePickup.h"

#include "Components/SphereComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogBasePickup, All, All);


AUECBasePickup::AUECBasePickup()
{
	PrimaryActorTick.bCanEverTick = true;
	CollisionComponent = CreateDefaultSubobject<USphereComponent>("SphereComponent");
	CollisionComponent->InitSphereRadius(50.f);
	CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	SetRootComponent(CollisionComponent);
}

void AUECBasePickup::BeginPlay()
{
	Super::BeginPlay();
	check(CollisionComponent);
	GenerateRotationYaw();
}

void AUECBasePickup::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);
	const auto Pawn = Cast<APawn>(OtherActor);
	if (GivePickupTo(Pawn))
	{
		PickUpWasTaken();
	}
}

void AUECBasePickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	AddActorLocalRotation(FRotator(0.f,RotationYaw, 0.f));
}

bool AUECBasePickup::CouldBeTaken() const
{
	return GetWorldTimerManager().IsTimerActive(RespawnTimerHandle);
}

bool AUECBasePickup::GivePickupTo(APawn* PlayerPawn)
{
	return false;
}

void AUECBasePickup::PickUpWasTaken()
{
	CollisionComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	if (GetRootComponent())
	{
		GetRootComponent()->SetVisibility(false, true);
	}
	GetWorldTimerManager().SetTimer(RespawnTimerHandle, this, &AUECBasePickup::RespawnPickup, RespawnTime);
}

void AUECBasePickup::RespawnPickup()
{
	GenerateRotationYaw();
	CollisionComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	if (GetRootComponent())
	{
		GetRootComponent()->SetVisibility(true, true);
	}
}

void AUECBasePickup::GenerateRotationYaw()
{
	const auto Direction = FMath::RandBool()? 1.f:-1.f;
	RotationYaw = FMath::RandRange(1.f, 2.f) * Direction;
}
