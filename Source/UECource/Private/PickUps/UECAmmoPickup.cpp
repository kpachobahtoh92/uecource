// Unreal Engine course project


#include "PickUps/UECAmmoPickup.h"

#include "UECUtils.h"
#include "Player/Components/UECHealthComponent.h"
#include "Player/Components/UECWeaponComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogAmmoPickup, All, All);

AUECAmmoPickup::AUECAmmoPickup()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void AUECAmmoPickup::BeginPlay()
{
	Super::BeginPlay();
	
}

void AUECAmmoPickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

bool AUECAmmoPickup::GivePickupTo(APawn* PlayerPawn)
{
	const auto HealthComponent = UECUtils::GetUECPlayerComponent<UUECHealthComponent>(PlayerPawn);
	if (!HealthComponent || HealthComponent->IsDead())
	{
		return false;
	}
	const auto WeaponComponent = UECUtils::GetUECPlayerComponent<UUECWeaponComponent>(PlayerPawn);
	if (!WeaponComponent)
	{
		return false;
	}
	return WeaponComponent->TryToAddAmmo(WeaponType, ClipsAmount);
}

