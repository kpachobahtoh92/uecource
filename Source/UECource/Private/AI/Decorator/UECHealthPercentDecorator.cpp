// Unreal Engine course project


#include "AI/Decorator/UECHealthPercentDecorator.h"

#include "AIController.h"
#include "Player/Components/UECHealthComponent.h"

UUECHealthPercentDecorator::UUECHealthPercentDecorator()
{
	NodeName = "Health percent";
}

bool UUECHealthPercentDecorator::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	const auto Controller = OwnerComp.GetAIOwner();
	if (!Controller)
	{
		return false;
	}
	const auto HealthComponent = Controller->GetPawn()->GetComponentByClass<UUECHealthComponent>();
	if (!HealthComponent || HealthComponent->IsDead())
	{
		return false;
	}
	return HealthComponent->GetHealthPercent() <= HealthPercent;
}
