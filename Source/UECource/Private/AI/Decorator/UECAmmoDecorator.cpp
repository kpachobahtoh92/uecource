// Unreal Engine course project


#include "AI/Decorator/UECAmmoDecorator.h"

#include "AIController.h"
#include "Player/Components/UECAIWeaponComponent.h"

UUECAmmoDecorator::UUECAmmoDecorator()
{
	NodeName = "Need ammo";
}

bool UUECAmmoDecorator::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	AAIController* Controller = OwnerComp.GetAIOwner();
	if (!Controller)
	{
		return false;
	}
	UUECAIWeaponComponent* WeaponComponent = Controller->GetPawn()->GetComponentByClass<UUECAIWeaponComponent>();
	if (!WeaponComponent)
	{
		return false;
	}
	return WeaponComponent->IsAmmoEmpty(WeaponToCheck);
}
