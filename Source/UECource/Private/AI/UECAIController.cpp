// Unreal Engine course project


#include "AI/UECAIController.h"

#include "AI/UECAICharacter.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Player/Components/UECAIPerceptionComponent.h"
#include "Player/Components/UECRespawnComponent.h"


// Sets default values
AUECAIController::AUECAIController()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RespawnComponent = CreateDefaultSubobject<UUECRespawnComponent>("RespawnComponent");
	UECAIPerceptionComponent = CreateDefaultSubobject<UUECAIPerceptionComponent>("UECAIPerceptionComponent");
	SetPerceptionComponent(*UECAIPerceptionComponent);
	

	bWantsPlayerState = true;
}

// Called when the game starts or when spawned
void AUECAIController::BeginPlay()
{
	Super::BeginPlay();
	
}

void AUECAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	const auto UECCharacter = Cast<AUECAICharacter>(InPawn);
	if (UECCharacter)
	{
		RunBehaviorTree(UECCharacter->BehaviorTreeAsset);
	}
}

// Called every frame
void AUECAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	const auto AimActor = GetFocusOnActor();
	SetFocus(AimActor);
}

AActor* AUECAIController::GetFocusOnActor() const
{
	if (!GetBlackboardComponent())
	{
		return nullptr;
	}
	return Cast<AActor>(GetBlackboardComponent()->GetValueAsObject(FocusOnKeyName));
}

