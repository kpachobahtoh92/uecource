// Unreal Engine course project


#include "AI/Services/UECFindEnemyService.h"

#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Player/Components/UECAIPerceptionComponent.h"

UUECFindEnemyService::UUECFindEnemyService()
{
	NodeName = "Find Enemy";
}

void UUECFindEnemyService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	const auto BlackboardComponent = OwnerComp.GetBlackboardComponent();
	if (BlackboardComponent)
	{
		const auto Controller = OwnerComp.GetAIOwner();
		const auto PerceptionComponent = Controller->GetComponentByClass<UUECAIPerceptionComponent>();
		if (PerceptionComponent)
		{
			BlackboardComponent->SetValueAsObject(EnemyActorKey.SelectedKeyName, PerceptionComponent->GetClosestEnemy());
		}
	}
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}
