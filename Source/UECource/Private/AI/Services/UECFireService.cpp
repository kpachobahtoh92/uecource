// Unreal Engine course project


#include "AI/Services/UECFireService.h"
#include "Player/Components/UECWeaponComponent.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"

UUECFireService::UUECFireService()
{
	NodeName = "Fire";
}

void UUECFireService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	const auto Controller = OwnerComp.GetAIOwner();
	const auto BlackboardComponent = OwnerComp.GetBlackboardComponent();

	const auto HasAim = BlackboardComponent && BlackboardComponent->GetValueAsObject(EnemyActorKey.SelectedKeyName);

	if (Controller)
	{
		const auto WeaponComponent = Cast<AActor>(Controller->GetPawn())->GetComponentByClass<UUECWeaponComponent>();
		if (WeaponComponent)
		{
			HasAim? WeaponComponent->StartFire() : WeaponComponent->StopFire();
		}
	}
	
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}
