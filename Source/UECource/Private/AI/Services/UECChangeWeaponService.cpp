// Unreal Engine course project


#include "AI/Services/UECChangeWeaponService.h"

#include "AIController.h"
#include "Player/Components/UECWeaponComponent.h"

UUECChangeWeaponService::UUECChangeWeaponService()
{
	NodeName = "Change weapon";
	Interval = 15.f;
	RandomDeviation = 5.f;
}

void UUECChangeWeaponService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	const auto Controller = OwnerComp.GetAIOwner();
	if (!Controller && Probability > 0 && FMath::FRand() <= Probability)
	{
		return;
	}
	const auto WeaponComponent = Controller->GetPawn()->GetComponentByClass<UUECWeaponComponent>();
	if (!WeaponComponent)
	{
		return;
	}
	WeaponComponent->NextWeapon();
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}
