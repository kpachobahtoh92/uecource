// Unreal Engine course project


#include "AI/Tasks/UECNextLocationTask.h"

#include "AIController.h"
#include "NavigationSystem.h"
#include "BehaviorTree/BlackboardComponent.h"

UUECNextLocationTask::UUECNextLocationTask()
{
	NodeName = "Next Location";
}

EBTNodeResult::Type UUECNextLocationTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const auto Controller = OwnerComp.GetAIOwner();
	const auto Blackboard = OwnerComp.GetBlackboardComponent();
	if (!Controller || !Blackboard)
	{
		return  EBTNodeResult::Failed;
	}
	const auto Pawn = Controller->GetPawn();
	if (!Pawn)
	{
		return  EBTNodeResult::Failed;
	}
	const auto NavigationSystem = UNavigationSystemV1::GetCurrent(Pawn);
	if (!NavigationSystem)
	{
		return  EBTNodeResult::Failed;
	}
	FNavLocation NavLocation;
	auto Location = Pawn->GetActorLocation();
	if (!SelfCenter)
	{
		auto CenterActor = Cast<AActor>(Blackboard->GetValueAsObject(CenterActorKey.SelectedKeyName));
		if (!CenterActor)
		{
			return  EBTNodeResult::Failed;
		}
		Location = CenterActor->GetActorLocation();
	}
	const auto Found = NavigationSystem->GetRandomReachablePointInRadius(Location, Radius, NavLocation);
	if (!Found)
	{
		return  EBTNodeResult::Failed;
	}
	Blackboard->SetValueAsVector(AimLocationKey.SelectedKeyName, NavLocation.Location);
	return  EBTNodeResult::Succeeded;
}
