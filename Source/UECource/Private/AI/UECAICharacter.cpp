// Unreal Engine course project


#include "AI/UECAICharacter.h"

#include "BrainComponent.h"
#include "AI/UECAIController.h"
#include "Components/WidgetComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Player/Components/UECAIWeaponComponent.h"
#include "Player/Components/UECHealthComponent.h"
#include "UI/UECHealthBarWidget.h"


// Sets default values
AUECAICharacter::AUECAICharacter(const FObjectInitializer& ObjInit)
:Super(ObjInit.SetDefaultSubobjectClass<UUECAIWeaponComponent>("WeaponComponent"))
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	AutoPossessAI = EAutoPossessAI::Disabled;
	AIControllerClass = AUECAIController::StaticClass();

	bUseControllerRotationYaw = false;
	if (GetCharacterMovement())
	{
		GetCharacterMovement()->bUseControllerDesiredRotation = true;
		GetCharacterMovement()->RotationRate = FRotator(0.f,200.f,0.f);
	}

	HealthBarWidgetComponent = CreateDefaultSubobject<UWidgetComponent>("HealthBarWidgetComponent");
	HealthBarWidgetComponent->SetupAttachment(GetRootComponent());
	HealthBarWidgetComponent->SetWidgetSpace(EWidgetSpace::Screen);
	HealthBarWidgetComponent->SetDrawAtDesiredSize(true);
	const FVector Location = FVector(0.f,0.f,120.f);
	HealthBarWidgetComponent->SetRelativeLocation(Location);
}

// Called when the game starts or when spawned
void AUECAICharacter::BeginPlay()
{
	Super::BeginPlay();
	check(HealthBarWidgetComponent);
}

// Called every frame
void AUECAICharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	UpdateHealthWidgetVisibility();
}

// Called to bind functionality to input
void AUECAICharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AUECAICharacter::OnHealthChange(float Health, float HealthDelta)
{
	Super::OnHealthChange(Health, HealthDelta);

	const auto HealthBarWidget = Cast<UUECHealthBarWidget>(HealthBarWidgetComponent->GetUserWidgetObject());
	if (!HealthBarWidget)
	{
		return;
	}
	HealthBarWidget->SetHealthPercent(HealthComponent->GetHealthPercent());
}

void AUECAICharacter::UpdateHealthWidgetVisibility()
{
	if (!GetWorld() || !GetWorld()->GetFirstPlayerController() || !GetWorld()->GetFirstPlayerController()->GetPawnOrSpectator())
	{
		return;
	}
	const auto PlayerLocation = GetWorld()->GetFirstPlayerController()->GetPawnOrSpectator()->GetActorLocation();
	const auto Distance = FVector::Distance(PlayerLocation, GetActorLocation());
	HealthBarWidgetComponent->SetVisibility(Distance < HealthVisibilityDistance, true);
}

void AUECAICharacter::OnDeath()
{
	Super::OnDeath();
	const auto UECController = Cast<AAIController>(Controller);
	if (UECController && UECController->GetBrainComponent())
	{
		UECController->GetBrainComponent()->Cleanup();
	}
}

