// Unreal Engine cource project.

#include "UECGameModeBase.h"

#include "AIController.h"
#include "EngineUtils.h"
#include "UECGameInstance.h"
#include "Player/UECPlayerController.h"
#include "Player/UECBaseCharacter.h"
#include "Player/UECPlayerState.h"
#include "Player/Components/UECRespawnComponent.h"
#include "UI/UECGameHUD.h"

DEFINE_LOG_CATEGORY_STATIC(LogUECGameMode, All, All);

AUECGameModeBase::AUECGameModeBase()
{
	DefaultPawnClass = AUECBaseCharacter::StaticClass();
	PlayerControllerClass = AUECPlayerController::StaticClass();
	HUDClass = AUECGameHUD::StaticClass();
	PlayerStateClass = AUECPlayerState::StaticClass();
}

void AUECGameModeBase::StartPlay()
{
	Super::StartPlay();

	SpawnBots();
	CreateTeamInfo();
	CurrentRound = 1;
	StartRound();
	SetMatchState(MS_InProgress);
}

UClass* AUECGameModeBase::GetDefaultPawnClassForController_Implementation(AController* InController)
{
	if (InController && InController->IsA<AAIController>())
	{
		return AIPawnClass;
	}
	return Super::GetDefaultPawnClassForController_Implementation(InController);
}

void AUECGameModeBase::Killed(AController* KillerController, AController* VictimController)
{
	AUECPlayerState* KillerPlayerState = KillerController
		                                     ? KillerController->GetPlayerState<AUECPlayerState>()
		                                     : nullptr;
	AUECPlayerState* VictimPlayerState = VictimController
		                                     ? VictimController->GetPlayerState<AUECPlayerState>()
		                                     : nullptr;
	if (KillerPlayerState)
	{
		KillerPlayerState->AddKillsNum();
	}
	if (VictimPlayerState)
	{
		VictimPlayerState->AddDeathsNum();
	}
	StartRespawn(VictimController);
}

void AUECGameModeBase::RespawnRequest(AController* Controller)
{
	ResetOnePlayer(Controller);
}

bool AUECGameModeBase::SetPause(APlayerController* PC, FCanUnpause CanUnpauseDelegate)
{
	bool bPause = Super::SetPause(PC, CanUnpauseDelegate);
	if (bPause)
	{
		SetMatchState(MS_Pause);
	}
	return bPause;
}

bool AUECGameModeBase::ClearPause()
{
	bool bClearPause = Super::ClearPause();
	if (bClearPause)
	{
		SetMatchState(MS_InProgress);
	}
	return bClearPause;
}

void AUECGameModeBase::StartRespawn(AController* Controller)
{
	if (RoundCountDown > 10 + GameData.RoundTime)
	{
		return;
	}
	UUECRespawnComponent* RespawnComponent = Controller->GetComponentByClass<UUECRespawnComponent>();
	if (!RespawnComponent)
	{
		return;
	}
	RespawnComponent->Respawn(GameData.RespawnTime);
}

void AUECGameModeBase::GameOver()
{
	UE_LOG(LogUECGameMode, Display, TEXT("=================Game Over================="));
	LogPlayerInfo();
	for (auto Pawn : TActorRange<APawn>(GetWorld()))
	{
		if (!Pawn)
		{
			continue;
		}
		Pawn->TurnOff();
		Pawn->DisableInput(nullptr);
	}
	SetMatchState(MS_GameOver);
}

void AUECGameModeBase::SetMatchState(EUECMatchState State)
{
	if (MatchState == State)
	{
		return;
	}
	MatchState = State;
	OnMatchStateChanged.Broadcast(MatchState);
}

void AUECGameModeBase::LogPlayerInfo()
{
	if (!GetWorld())
	{
		return;
	}
	for (auto ControllerIterator = GetWorld()->GetControllerIterator(); ControllerIterator; ++ControllerIterator)
	{
		const AController* Controller = ControllerIterator->Get();
		if (!Controller)
		{
			continue;
		}
		AUECPlayerState* PlayerState = Controller->GetPlayerState<AUECPlayerState>();
		if (!PlayerState)
		{
			continue;
		}
		PlayerState->LogInfo();
	}
}

void AUECGameModeBase::SpawnBots()
{
	if (!GetWorld())
	{
		return;
	}
	for (int32 i = 0; i < GameData.PlayersNum - 1; i++)
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		AAIController* UECAIController = GetWorld()->SpawnActor<AAIController>(AIControllerClass, SpawnParameters);
		RestartPlayer(UECAIController);
	}
}

void AUECGameModeBase::StartRound()
{
	RoundCountDown = GameData.RoundTime;
	GetWorldTimerManager().SetTimer(GameRoundTimerHandle, this, &AUECGameModeBase::GameTimerUpdate, 1.f, true);
}

void AUECGameModeBase::GameTimerUpdate()
{
	UE_LOG(LogUECGameMode, Display, TEXT("Time: %i / Round %i:%i"), RoundCountDown, CurrentRound, GameData.RoundNum);

	if (--RoundCountDown == 0)
	{
		GetWorldTimerManager().ClearTimer(GameRoundTimerHandle);
		if (CurrentRound + 1 <= GameData.RoundNum)
		{
			++CurrentRound;
			ResetPlayers();
			StartRound();
		}
		else
		{
			GameOver();
		}
	}
}

void AUECGameModeBase::ResetPlayers()
{
	if (!GetWorld())
	{
		return;
	}
	for (auto Controller = GetWorld()->GetControllerIterator(); Controller; ++Controller)
	{
		ResetOnePlayer(Controller->Get());
	}
}

void AUECGameModeBase::ResetOnePlayer(AController* Controller)
{
	if (Controller && Controller->GetPawn())
	{
		Controller->GetPawn()->Reset();
	}
	RestartPlayer(Controller);
	SetPlayerColor(Controller);
}

void AUECGameModeBase::CreateTeamInfo()
{
	if (!GetWorld())
	{
		return;
	}
	int32 TeamID = 1;
	for (auto ControllerIterator = GetWorld()->GetControllerIterator(); ControllerIterator; ++ControllerIterator)
	{
		AController* Controller = ControllerIterator->Get();
		if (!Controller)
		{
			continue;
		}
		AUECPlayerState* PlayerState = Controller->GetPlayerState<AUECPlayerState>();
		if (!PlayerState)
		{
			continue;
		}
		PlayerState->SetTeamID(TeamID);
		PlayerState->SetTeamColor(DetermineColorByTeamID(TeamID));
		PlayerState->SetPlayerName(Controller->IsPlayerController() ? "Player" : "Bot");
		SetPlayerColor(Controller);
		TeamID = TeamID == 1 ? 2 : 1;
	}
}

FLinearColor AUECGameModeBase::DetermineColorByTeamID(int32 TeamID) const
{
	if (TeamID - 1 < GameData.TeamColors.Num())
	{
		return GameData.TeamColors[TeamID - 1];
	}
	UE_LOG(LogUECGameMode, Warning, TEXT("No color for team id: %i, set to default: %s"), TeamID,
	       *GameData.DefaultTeamColor.ToString())
	return GameData.DefaultTeamColor;
}

void AUECGameModeBase::SetPlayerColor(AController* Controller)
{
	if (!Controller)
	{
		return;
	}
	AUECBaseCharacter* Character = Cast<AUECBaseCharacter>(Controller->GetPawn());
	if (!Character)
	{
		return;
	}
	AUECPlayerState* PlayerState = Controller->GetPlayerState<AUECPlayerState>();
	if (!PlayerState)
	{
		return;
	}
	Character->SetPlayerColor(PlayerState->GetTeamColor());
}
