// Unreal Engine course project


#include "Entry/UECEntryGameModeBase.h"

#include "Entry/UECEntryPlayerController.h"
#include "Entry/UI/UECEntryHUD.h"

AUECEntryGameModeBase::AUECEntryGameModeBase()
{
	PlayerControllerClass = AUECEntryPlayerController::StaticClass();
	HUDClass = AUECEntryHUD::StaticClass();
}
