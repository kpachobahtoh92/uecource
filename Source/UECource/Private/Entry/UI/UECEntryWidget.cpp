// Unreal Engine course project


#include "Entry/UI/UECEntryWidget.h"

#include "UECGameInstance.h"
#include "Components/Button.h"
#include "Components/HorizontalBox.h"
#include "Entry/UI/UECLevelItemWidget.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(LogUECEntryWidget, All, All);


void UUECEntryWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	if (StartGameButton)
	{
		StartGameButton->OnClicked.AddDynamic(this, &UUECEntryWidget::OnStartGame);
	}
	InitLevelItems();
}

void UUECEntryWidget::InitLevelItems()
{
	UUECGameInstance* UECGameInstance = GetUECGameInstance();
	if (!UECGameInstance)
	{
		return;
	}
	checkf(UECGameInstance->GetLevelsData().Num() != 0, TEXT("Levels Data must not be empty!"))

	if (!LevelItemsBox)
	{
		return;
	}
	LevelItemsBox->ClearChildren();
	for (auto LevelData : UECGameInstance->GetLevelsData())
	{
		UUECLevelItemWidget* LevelItemWidget = CreateWidget<UUECLevelItemWidget>(GetWorld(), LevelItemWidgetClass);
		if (!LevelItemWidget)
		{
			return;
		}
		LevelItemWidget->SetLevelData(LevelData);
		LevelItemWidget->OnLevelSelected.AddUObject(this, &UUECEntryWidget::OnLevelSelected);
		LevelItemsBox->AddChild(LevelItemWidget);
		LevelItemWidgets.Add(LevelItemWidget);
	}
	if (UECGameInstance->GetStartupLevel().LevelName.IsNone())
	{
		OnLevelSelected(UECGameInstance->GetLevelsData()[0]);
	}
	else
	{
		OnLevelSelected(UECGameInstance->GetStartupLevel());
	}
}

void UUECEntryWidget::OnLevelSelected(const FLevelData& Data)
{
	UUECGameInstance* UECGameInstance = GetUECGameInstance();
	if (!UECGameInstance)
	{
		return;
	}
	UECGameInstance->SetStartupLevel(Data);
	for (auto Element : LevelItemWidgets)
	{
		if (Element)
		{
			bool bCond = Data.LevelName == Element->GetLevelData().LevelName;
			Element->SetSelected(bCond);
		}
	}
}

UUECGameInstance* UUECEntryWidget::GetUECGameInstance() const
{
	if (!GetWorld())
	{
		return nullptr;
	}
	return GetWorld()->GetGameInstance<UUECGameInstance>();
}

void UUECEntryWidget::OnStartGame()
{
	UUECGameInstance* UECGameInstance = GetUECGameInstance();
	if (!UECGameInstance)
	{
		return;
	}
	UGameplayStatics::OpenLevel(this, UECGameInstance->GetStartupLevel().LevelName);
}
