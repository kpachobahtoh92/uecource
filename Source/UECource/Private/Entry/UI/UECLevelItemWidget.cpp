// Unreal Engine course project


#include "Entry/UI/UECLevelItemWidget.h"

#include "Components/Button.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"

void UUECLevelItemWidget::SetLevelData(const FLevelData& Data)
{
	LevelData = Data;

	if (LevelNameTextBlock)
	{
		LevelNameTextBlock->SetText(FText::FromName(Data.LevelDisplayName));
	}
	if (LevelImage)
	{
		LevelImage->SetBrushFromTexture(Data.LevelThumb);
	}
}

void UUECLevelItemWidget::SetSelected(bool bIsSelected)
{
	if (FrameImage)
	{
		FrameImage->SetVisibility(bIsSelected? ESlateVisibility::Visible : ESlateVisibility::Hidden);
	}
}

void UUECLevelItemWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	if (LevelSelectButton)
	{
		LevelSelectButton->OnClicked.AddDynamic(this, &UUECLevelItemWidget::OnLevelItemClicked);
	}
}

void UUECLevelItemWidget::OnLevelItemClicked()
{
	OnLevelSelected.Broadcast(LevelData);
}
