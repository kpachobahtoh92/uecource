// Unreal Engine course project


#include "Entry/UI/UECEntryHUD.h"
#include "Blueprint/UserWidget.h"

void AUECEntryHUD::BeginPlay()
{
	Super::BeginPlay();
	if (EntryUserWidgetClass && GetWorld())
	{
		UUserWidget* EntryWidget = CreateWidget<UUserWidget>(GetWorld(), EntryUserWidgetClass);
		if (EntryWidget)
		{
			EntryWidget->AddToViewport();
		}
	}
}
