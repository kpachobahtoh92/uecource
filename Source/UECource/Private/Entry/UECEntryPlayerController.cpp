// Unreal Engine course project


#include "Entry/UECEntryPlayerController.h"

#include "UECGameInstance.h"

void AUECEntryPlayerController::BeginPlay()
{
	Super::BeginPlay();
	SetInputMode(FInputModeUIOnly());
	bShowMouseCursor = true;
}
