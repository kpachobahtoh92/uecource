// Unreal Engine course project


#include "Weapon/UECLauncherWeapon.h"
#include "Weapon/UECProjectile.h"

AUECLauncherWeapon::AUECLauncherWeapon()
{
	PrimaryActorTick.bCanEverTick = false;
}



void AUECLauncherWeapon::StartFire()
{
	Super::StartFire();
	MakeShot();
}

void AUECLauncherWeapon::MakeShot()
{
	Super::MakeShot();

	if (!GetWorld() || IsAmmoEmpty())
	{
		return;
	}

	FVector TraceStart, TraceEnd;
	if (!GetTraceData(TraceStart, TraceEnd))
	{
		return;
	}
	FHitResult HitResult;
	MakeHit(HitResult, TraceStart, TraceEnd);

	const FVector EndPoint = HitResult.bBlockingHit? HitResult.ImpactPoint : TraceEnd;
	const FVector Direction = (EndPoint - GetMuzzleWorldLocation()).GetSafeNormal();
	
	const FTransform SpawnTransform(FRotator::ZeroRotator, GetMuzzleWorldLocation());
	AUECProjectile* Projectile = GetWorld()->SpawnActorDeferred<AUECProjectile>(ProjectileClass, SpawnTransform);
	if (Projectile)
	{
		Projectile->SetShootDirection(Direction);
		Projectile->SetOwner(GetOwner());
		Projectile->FinishSpawning(SpawnTransform);
	}
	DecreaseAmmo();
	SpawnMuzzleFX();
}
