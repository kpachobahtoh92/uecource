// Unreal Engine course project

#include "Weapon/UECProjectile.h"

#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Weapon/Components/UECWeaponFXComponent.h"

AUECProjectile::AUECProjectile()
{
	PrimaryActorTick.bCanEverTick = false;
	CollisionComponent = CreateDefaultSubobject<USphereComponent>("SphereComponent");
	CollisionComponent->InitSphereRadius(5.f);
	CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComponent->SetCollisionResponseToAllChannels(ECR_Block);
	CollisionComponent->bReturnMaterialOnMove = true;
	SetRootComponent(CollisionComponent);

	WeaponFXComponent = CreateDefaultSubobject<UUECWeaponFXComponent>("WeaponFXComponent");

	MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>("ProjectileMovementComponent");
	MovementComponent->InitialSpeed = 2000.f;
	MovementComponent->ProjectileGravityScale = 0.f;
}

void AUECProjectile::OnProjectileHit(UPrimitiveComponent* HitComponent, AActor* OtherActor,
                                     UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!GetWorld())
	{
		return;
	}
	MovementComponent->StopMovementImmediately();

	UGameplayStatics::ApplyRadialDamage(GetWorld(), DamageAmount, GetActorLocation(), DamageRadius,
	                                    UDamageType::StaticClass(), {GetOwner()}, this, GetController(), bIsDoFullDamage);
	//DrawDebugSphere(GetWorld(), GetActorLocation(), DamageRadius,24, FColor::Red,false, 5.f);
	WeaponFXComponent->PlayImpactFX(Hit);
	Destroy();
}

void AUECProjectile::BeginPlay()
{
	Super::BeginPlay();
	check(MovementComponent);
	check(CollisionComponent)
	check(WeaponFXComponent);
	MovementComponent->Velocity = ShootDirection * MovementComponent->InitialSpeed;
	CollisionComponent->OnComponentHit.AddDynamic(this, &AUECProjectile::OnProjectileHit);
	CollisionComponent->IgnoreActorWhenMoving(GetOwner(), true);

	SetLifeSpan(LifeSeconds);
}

AController* AUECProjectile::GetController() const
{
	APawn* const Pawn = Cast<APawn>(GetOwner());
	return Pawn? Pawn->GetController(): nullptr;
}
