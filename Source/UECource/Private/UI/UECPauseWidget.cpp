// Unreal Engine course project


#include "UI/UECPauseWidget.h"

#include "Components/Button.h"
#include "GameFramework/GameModeBase.h"

void UUECPauseWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	if (ClearPauseButton)
	{
		ClearPauseButton->OnClicked.AddDynamic(this, &UUECPauseWidget::OnClearPause);
	}
}

void UUECPauseWidget::OnClearPause()
{
	if (!GetWorld() || !GetWorld()->GetAuthGameMode())
	{
		return;
	}
	GetWorld()->GetAuthGameMode()->ClearPause();
}
