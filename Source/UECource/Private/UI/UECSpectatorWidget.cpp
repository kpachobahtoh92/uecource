// Unreal Engine course project


#include "UI/UECSpectatorWidget.h"

#include "Player/Components/UECRespawnComponent.h"

bool UUECSpectatorWidget::GetRespawnTime(int32& CountDownTime) const
{
	UUECRespawnComponent* RespawnComponent = GetOwningPlayer()->GetComponentByClass<UUECRespawnComponent>();
	if (!RespawnComponent || !RespawnComponent->IsRespawnInProgress())
	{
		return false;
	}
	CountDownTime = RespawnComponent->GetRespawnCountDown();
	return true;
}
