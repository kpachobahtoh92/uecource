// Unreal Engine course project


#include "UI/UECMainMenuButton.h"

#include "UECGameInstance.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(LogUECMainMenuButton, All, All);

void UUECMainMenuButton::Clicked()
{
	Super::Clicked();
	if (!GetWorld())
	{
		return;
	}
	UUECGameInstance* GameInstance = GetWorld()->GetGameInstance<UUECGameInstance>();
	if (!GameInstance)
	{
		return;
	}
	if (GameInstance->GetEntryLevelName().IsNone())
	{
		UE_LOG(LogUECMainMenuButton, Error, TEXT("Level name is NONE"))
		return;
	}
	UGameplayStatics::OpenLevel(this, GameInstance->GetEntryLevelName());
}
