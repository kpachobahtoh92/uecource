// Unreal Engine course project


#include "UI/UECGameHUD.h"

#include "UECGameModeBase.h"
#include "Blueprint/UserWidget.h"
#include "Engine/Canvas.h"

DEFINE_LOG_CATEGORY_STATIC(LogUECGameHUD, All, All);

void AUECGameHUD::DrawHUD()
{
	Super::DrawHUD();
	//DrawCrosshair();
}

void AUECGameHUD::BeginPlay()
{
	Super::BeginPlay();

	GameWidgets.Add(EUECMatchState::MS_InProgress, CreateWidget<UUserWidget>(GetWorld(), PlayerHUDWidgetClass));
	GameWidgets.Add(EUECMatchState::MS_Pause, CreateWidget<UUserWidget>(GetWorld(), PauseWidgetClass));
	GameWidgets.Add(EUECMatchState::MS_GameOver, CreateWidget<UUserWidget>(GetWorld(), GameOverWidgetClass));

	for (auto GameWidgetPair : GameWidgets)
	{
		UUserWidget* GameWidget = GameWidgetPair.Value;
		if (!GameWidget)
		{
			continue;
		}
		GameWidget->AddToViewport();
		GameWidget->SetVisibility(ESlateVisibility::Hidden);
	}
	
	if (GetWorld())
	{
		AUECGameModeBase* GameModeBase = Cast<AUECGameModeBase>(GetWorld()->GetAuthGameMode());
		if (GameModeBase)
		{
			GameModeBase->OnMatchStateChanged.AddUObject(this, &AUECGameHUD::OnMatchStateChange);
		}
	}
}

void AUECGameHUD::DrawCrosshair()
{
	const TInterval<float> Center(Canvas->SizeX * 0.5, Canvas->SizeY * 0.5);
	const float HalfLineSize = 10.f;
	const float LineThickness = 2.f;
	const FLinearColor LinearColor = FLinearColor::Green;
	DrawLine(Center.Min - HalfLineSize, Center.Max, Center.Min + HalfLineSize, Center.Max, LinearColor, LineThickness);
	DrawLine(Center.Min, Center.Max - HalfLineSize, Center.Min, Center.Max + HalfLineSize, LinearColor, LineThickness);
}

void AUECGameHUD::OnMatchStateChange(EUECMatchState State)
{
	if (CurrentWidget)
	{
		CurrentWidget->SetVisibility(ESlateVisibility::Hidden);
	}
	if (GameWidgets.Contains(State))
	{
		CurrentWidget = GameWidgets[State];
	}
	if (CurrentWidget)
	{
		CurrentWidget->SetVisibility(ESlateVisibility::Visible);
	}
	UE_LOG(LogUECGameHUD, Display, TEXT("Match state changed: %s"), *UEnum::GetValueAsString(State))
}
