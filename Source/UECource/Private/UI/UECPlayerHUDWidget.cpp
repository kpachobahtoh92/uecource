// Unreal Engine course project


#include "UI/UECPlayerHUDWidget.h"

#include "Player/Components/UECHealthComponent.h"
#include "Player/Components/UECWeaponComponent.h"
#include "UECUtils.h"
#include "Components/ProgressBar.h"

void UUECPlayerHUDWidget::OnNewPawn(APawn* Pawn)
{
	const auto HealthComponent = UECUtils::GetUECPlayerComponent<UUECHealthComponent>(Pawn);
	if (HealthComponent)
	{
		HealthComponent->OnHealthChange.AddUObject(this, &UUECPlayerHUDWidget::OnHealthChanged);
	}
	UpdateHealthBar();
}

float UUECPlayerHUDWidget::GetHealthPercent() const
{
	const auto HealthComponent = UECUtils::GetUECPlayerComponent<UUECHealthComponent>(GetOwningPlayerPawn());
	if (!HealthComponent)
	{
		return 0.f;
	}
	return HealthComponent->GetHealthPercent();
}

bool UUECPlayerHUDWidget::GetCurrentWeaponUIData(FWeaponUIData& UIData) const
{
	const auto WeaponComponent = UECUtils::GetUECPlayerComponent<UUECWeaponComponent>(GetOwningPlayerPawn());
	if (!WeaponComponent)
	{
		return false;
	}
	return WeaponComponent->GetCurrentWeaponUIData(UIData);
}

bool UUECPlayerHUDWidget::GetCurrentWeaponAmmoData(FAmmoData& AmmoData) const
{
	UUECWeaponComponent* const WeaponComponent = UECUtils::GetUECPlayerComponent<UUECWeaponComponent>(
		GetOwningPlayerPawn());
	if (!WeaponComponent)
	{
		return false;
	}
	return WeaponComponent->GetCurrentWeaponAmmoData(AmmoData);
}

bool UUECPlayerHUDWidget::IsPlayerAlive() const
{
	const auto HealthComponent = UECUtils::GetUECPlayerComponent<UUECHealthComponent>(GetOwningPlayerPawn());
	return HealthComponent && !HealthComponent->IsDead();
}

bool UUECPlayerHUDWidget::IsPlayerSpectatig() const
{
	const auto Controller = GetOwningPlayer();
	return Controller && Controller->GetStateName() == NAME_Spectating;
}

int32 UUECPlayerHUDWidget::GetKillsNum() const
{
	const auto Controller = GetOwningPlayer();
	if (!Controller)
	{
		return 0;
	}
	const auto PlayerState = Cast<AUECPlayerState>(Controller->PlayerState);
	return PlayerState ? PlayerState->GetKillsNum() : 0;
}

void UUECPlayerHUDWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	if (GetOwningPlayer())
	{
		GetOwningPlayer()->GetOnNewPawnNotifier().AddUObject(this, &UUECPlayerHUDWidget::OnNewPawn);
		OnNewPawn(GetOwningPlayerPawn());
	}
}

void UUECPlayerHUDWidget::OnHealthChanged(float Health, float HealthDelta)
{
	if (HealthDelta < 0.f)
	{
		OnTakeAnyDamage();
	}
	UpdateHealthBar();
}

void UUECPlayerHUDWidget::UpdateHealthBar()
{
	if (HealthProgressBar)
	{
		HealthProgressBar->SetFillColorAndOpacity(GetHealthPercent() > PercentColorThreshold ? GoodColor : BadColor);
	}
}
