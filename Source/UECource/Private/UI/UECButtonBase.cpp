// Unreal Engine course project


#include "UI/UECButtonBase.h"

#include "Components/Button.h"
#include "Components/TextBlock.h"

void UUECButtonBase::Clicked()
{
	OnButtonBaseClicked.Broadcast();
}

void UUECButtonBase::Pressed()
{
	OnButtonBasePressed.Broadcast();
}

void UUECButtonBase::Hovered()
{
	OnButtonBaseHovered.Broadcast();
}

void UUECButtonBase::Unhovered()
{
	OnButtonBaseUnHovered.Broadcast();
}

void UUECButtonBase::Released()
{
	OnButtonBaseReleased.Broadcast();
}

void UUECButtonBase::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	BaseButton->OnClicked.AddDynamic(this, &UUECButtonBase::Clicked);
	BaseButton->OnPressed.AddDynamic(this, &UUECButtonBase::Pressed);
	BaseButton->OnReleased.AddDynamic(this, &UUECButtonBase::Released);
	BaseButton->OnHovered.AddDynamic(this, &UUECButtonBase::Hovered);
	BaseButton->OnUnhovered.AddDynamic(this, &UUECButtonBase::Unhovered);
}

void UUECButtonBase::NativePreConstruct()
{
	Super::NativePreConstruct();
	if (ButtonTextBlock)
	{
		ButtonTextBlock->SetText(ButtonText);
	}
}
