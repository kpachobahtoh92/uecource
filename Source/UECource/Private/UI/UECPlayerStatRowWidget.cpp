// Unreal Engine course project


#include "UI/UECPlayerStatRowWidget.h"

#include "Components/Image.h"

void UUECPlayerStatRowWidget::SetTeamColor(const FLinearColor& TeamColor)
{
	if (!TeamImage)
	{
		return;
	}
	TeamImage->SetColorAndOpacity(TeamColor);
}
