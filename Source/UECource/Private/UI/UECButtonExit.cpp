// Unreal Engine course project


#include "UI/UECButtonExit.h"

#include "Kismet/KismetSystemLibrary.h"

void UUECButtonExit::Clicked()
{
	Super::Clicked();
	if (GetWorld())
	{
		UKismetSystemLibrary::QuitGame(this, GetOwningPlayer(), EQuitPreference::Quit, true);
	}
}
