// Unreal Engine course project


#include "UI/UECGameOverWidget.h"

#include "UECGameModeBase.h"
#include "UECUtils.h"
#include "Components/Button.h"
#include "Components/VerticalBox.h"
#include "Kismet/GameplayStatics.h"
#include "Player/UECPlayerState.h"
#include "UI/UECPlayerStatRowWidget.h"

void UUECGameOverWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	if (GetWorld())
	{
		AUECGameModeBase* GameModeBase = Cast<AUECGameModeBase>(GetWorld()->GetAuthGameMode());
		if (GameModeBase)
		{
			GameModeBase->OnMatchStateChanged.AddUObject(this, &UUECGameOverWidget::OnMatchStateChange);
		}
	}
	if (ResetLevelButton)
	{
		ResetLevelButton->OnClicked.AddDynamic(this, &UUECGameOverWidget::OnResetLevel);
	}
}

void UUECGameOverWidget::UpdatePlayerStat()
{
	if (!GetWorld() || !PlayerStatBox)
	{
		return;
	}
	PlayerStatBox->ClearChildren();
	for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		AController* Controller = It->Get();
		if (!Controller)
		{
			continue;
		}
		AUECPlayerState* PlayerState = Controller->GetPlayerState<AUECPlayerState>();
		if (!PlayerState)
		{
			continue;
		}
		UUECPlayerStatRowWidget* PlayerStatRowWidget = CreateWidget<UUECPlayerStatRowWidget>(GetWorld(), PlayerStatRowWidgetClass);
		if (!PlayerStatRowWidget)
		{
			continue;
		}
		PlayerStatRowWidget->SetPlayerText(FText::FromString(PlayerState->GetPlayerName()));
		PlayerStatRowWidget->SetKillsText(UECUtils::TextFromInt(PlayerState->GetKillsNum()));
		PlayerStatRowWidget->SetDeathsText(UECUtils::TextFromInt(PlayerState->GetDeathsNum()));
		PlayerStatRowWidget->SetTeamText(UECUtils::TextFromInt(PlayerState->GetTeamID()));
		PlayerStatRowWidget->SetPlayerIndicatorVisibility(Controller->IsPlayerController()? ESlateVisibility::Visible : ESlateVisibility::Hidden);
		PlayerStatRowWidget->SetTeamColor(PlayerState->GetTeamColor());

		PlayerStatBox->AddChild(PlayerStatRowWidget);
	}
}

void UUECGameOverWidget::OnMatchStateChange(EUECMatchState MatchState)
{
	if (MatchState == MS_GameOver)
	{
		UpdatePlayerStat();
	}
}

void UUECGameOverWidget::OnResetLevel()
{
	const FName Testlevel = FName(UGameplayStatics::GetCurrentLevelName(this));
	UGameplayStatics::OpenLevel(this, Testlevel);
}
