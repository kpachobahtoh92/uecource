// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "Player/UECBaseCharacter.h"
#include "UECAICharacter.generated.h"

class UWidgetComponent;
class UBehaviorTree;

UCLASS()
class UECOURCE_API AUECAICharacter : public AUECBaseCharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AUECAICharacter(const FObjectInitializer& ObjInit);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI")
	UBehaviorTree* BehaviorTreeAsset;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI")
	float HealthVisibilityDistance = 1000.f;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UWidgetComponent* HealthBarWidgetComponent;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void OnHealthChange(float Health, float HealthDelta) override;

private:
	void UpdateHealthWidgetVisibility();

protected:
	virtual	void OnDeath() override;
};
