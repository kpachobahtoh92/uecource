// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "UECFireService.generated.h"

/**
 * 
 */
UCLASS()
class UECOURCE_API UUECFireService : public UBTService
{
	GENERATED_BODY()

public:
	UUECFireService();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	FBlackboardKeySelector EnemyActorKey;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;

};
