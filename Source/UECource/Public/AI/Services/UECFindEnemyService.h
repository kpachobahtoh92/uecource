// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "UECFindEnemyService.generated.h"

/**
 * 
 */
UCLASS()
class UECOURCE_API UUECFindEnemyService : public UBTService
{
	GENERATED_BODY()
public:
	UUECFindEnemyService();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	FBlackboardKeySelector EnemyActorKey;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
