// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "Runtime/AIModule/Classes/AIController.h"
#include "UECAIController.generated.h"

class UUECRespawnComponent;
class UUECAIPerceptionComponent;

UCLASS()
class UECOURCE_API AUECAIController : public AAIController
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AUECAIController();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void OnPossess(APawn* InPawn) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UUECAIPerceptionComponent* UECAIPerceptionComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UUECRespawnComponent* RespawnComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	FName FocusOnKeyName = "EnemyActor";

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	AActor* GetFocusOnActor() const;
};
