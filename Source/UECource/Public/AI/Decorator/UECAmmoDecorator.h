// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "UECAmmoDecorator.generated.h"

class AUECBaseWeapon;
/**
 * 
 */
UCLASS()
class UECOURCE_API UUECAmmoDecorator : public UBTDecorator
{
	GENERATED_BODY()
	
public:
	UUECAmmoDecorator();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	TSubclassOf<AUECBaseWeapon> WeaponToCheck;
	
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
};
