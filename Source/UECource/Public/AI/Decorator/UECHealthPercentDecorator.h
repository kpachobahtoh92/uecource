// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "UECHealthPercentDecorator.generated.h"

/**
 * 
 */
UCLASS()
class UECOURCE_API UUECHealthPercentDecorator : public UBTDecorator
{
	GENERATED_BODY()

public:
	UUECHealthPercentDecorator();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	float HealthPercent = 0.6f;
	
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
};
