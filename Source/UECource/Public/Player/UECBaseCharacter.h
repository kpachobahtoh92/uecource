// Unreal Engine cource project.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "UECBaseCharacter.generated.h"

class UUECWeaponComponent;
class UUECHealthComponent;


UCLASS()
class UECOURCE_API AUECBaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AUECBaseCharacter(const FObjectInitializer& ObjInit);

protected:
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UUECHealthComponent* HealthComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UUECWeaponComponent* WeaponComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
	UAnimMontage* DeathAnimMontage;
	
	UPROPERTY(EditDefaultsOnly, Category = "Movement")
	FVector2D LandedDamageVelocity = FVector2D(900.f, 1200.f);
	
	UPROPERTY(EditDefaultsOnly, Category = "Movement")
	FVector2D LandedDamage = FVector2D(10.f, 100.f);

	UPROPERTY(EditDefaultsOnly, Category = "Material")
	FName MaterialColorName = "Paint Color";

	virtual void OnDeath();

	virtual void OnHealthChange(float Health, float HealthDelta);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION(BlueprintCallable, Category = "Movement")
	float GetMovementDirection() const;
	
	void SetPlayerColor(FLinearColor& LinearColor);

private:

	void SpawnWeapon();

	UFUNCTION()
	void OnGroundLanded(const FHitResult& HitResult);

};
