// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "UECCoreTypes.h"
#include "GameFramework/PlayerController.h"
#include "UECPlayerController.generated.h"

class UUECRespawnComponent;
/**
 * 
 */
UCLASS()
class UECOURCE_API AUECPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	AUECPlayerController();

	virtual void SetupInputComponent() override;
	void OnMatchStateChange(EUECMatchState MatchState);
	virtual void BeginPlay() override;

private:
	void OnPauseGame();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UUECRespawnComponent* RespawnComponent;
};
