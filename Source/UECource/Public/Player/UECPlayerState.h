// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "UECPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class UECOURCE_API AUECPlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	void SetTeamID(int32 ID) { TeamID = ID; }
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PlayerState")
	int32 GetTeamID() const { return TeamID; }

	void SetTeamColor(FLinearColor ID) { TeamColor = ID; }
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PlayerState")
	FLinearColor& GetTeamColor() { return TeamColor; }

	void AddKillsNum() { ++KillsNum; }
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PlayerState")
	int32 GetKillsNum() const { return KillsNum; }

	void AddDeathsNum() { ++DeathsNum; }
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PlayerState")
	int32 GetDeathsNum() const { return DeathsNum; }

	void LogInfo();

private:
	int32 TeamID;
	FLinearColor TeamColor;

	int32 KillsNum = 0;
	int32 DeathsNum = 0;
};
