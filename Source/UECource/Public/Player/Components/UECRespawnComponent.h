// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "UECRespawnComponent.generated.h"


UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class UECOURCE_API UUECRespawnComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UUECRespawnComponent();

	void Respawn(int32 RespawnTime);

	int32 GetRespawnCountDown() const
	{
		return RespawnCountDown;
	}
	bool IsRespawnInProgress();

private:
	FTimerHandle RespawnTimerHandle;

	int32 RespawnCountDown = 0;

	void RespawnTimerUpdate();

protected:
	virtual void BeginPlay() override;
};
