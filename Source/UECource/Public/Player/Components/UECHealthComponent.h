// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "UECCoreTypes.h"
#include "UECHealthComponent.generated.h"

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class UECOURCE_API UUECHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UUECHealthComponent();

	float GetHealth() const {return Health;}

	UFUNCTION(BlueprintCallable, Category = "Health")
	FORCEINLINE bool IsDead() const { return  FMath::IsNearlyZero(Health);}

	UFUNCTION(BlueprintCallable, Category = "Health")
	float GetHealthPercent() const {return Health/MaxHealth;}

	bool TryToAddHealth(int32 HealthAmount);
	bool IsHealthFull() const;


	FOnDeath OnDeath;
	FOnHealthChange	OnHealthChange;


	

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health", meta = (ClampMin = "0.0", ClampMax = "1000.0"))
	float MaxHealth = 100.f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AutoHeal")
	bool bIsAutoHealEnabled = true;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AutoHeal", meta = (ClampMin = "0.0", EditCondition = "bIsAutoHealEnabled"))
	float AutoHealDelay = 3.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AutoHeal", meta = (ClampMin = "0.0", EditCondition = "bIsAutoHealEnabled"))
	float AutoHealFrequency = 1.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AutoHeal", meta = (ClampMin = "0.0", EditCondition = "bIsAutoHealEnabled"))
	float AutoHealHPCount = 1.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	TSubclassOf<UCameraShakeBase> CameraShake;

	FTimerHandle AutoHealTimerHandle;

	void AutoHeal();
	
	virtual void BeginPlay() override;

public:
private:
	float Health = 0.f;

	UFUNCTION()
	void OnTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType,
							   class AController* InstigatedBy, AActor* DamageCauser);

	void SetHealth(float NewHealth);

	void PlayCameraShake();

	void Killed(AController* KillerController);
};
