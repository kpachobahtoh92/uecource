// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "UECWeaponComponent.h"
#include "UECAIWeaponComponent.generated.h"


UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class UECOURCE_API UUECAIWeaponComponent : public UUECWeaponComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UUECAIWeaponComponent();

	virtual void StartFire() override;
	virtual void NextWeapon() override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
	                           FActorComponentTickFunction* ThisTickFunction) override;
};
