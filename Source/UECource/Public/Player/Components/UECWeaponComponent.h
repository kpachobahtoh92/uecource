// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Weapon/UECBaseWeapon.h"
#include "UECCoreTypes.h"
#include "UECWeaponComponent.generated.h"

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class UECOURCE_API UUECWeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UUECWeaponComponent();

	virtual void StartFire();
	void StopFire();
	virtual void NextWeapon();
	void Reload();

	bool GetCurrentWeaponUIData(FWeaponUIData& UIData) const;
	bool GetCurrentWeaponAmmoData(FAmmoData& AmmoData) const;
	bool IsAmmoEmpty(TSubclassOf<AUECBaseWeapon> WeaponClass) const;
	bool TryToAddAmmo(TSubclassOf<AUECBaseWeapon> WeaponClass, int32 ClipsAmount);

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	TArray<FWeaponData> WeaponData;
	
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	FName WeaponEquipSocketName = "WeaponSocket";

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	FName WeaponArmorySocketName = "ArmorySocket";

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
	UAnimMontage* EquipAnimMontage;
	
	virtual void BeginPlay() override;
	
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
	UPROPERTY()
	AUECBaseWeapon* CurrentWeapon = nullptr;

	UPROPERTY()
	TArray<AUECBaseWeapon*> Weapons;

	bool CanFire();
	bool CanEquip();
	void EquipWeapon(int32 WeaponIndex);
	int32 CurrentWeaponIndex = 0;

private:


	UPROPERTY()
	UAnimMontage* CurrentReloadAnimMontage = nullptr;

	bool EquipAnimInProgress = false;
	bool ReloadAnimInProgress = false;
	
	bool CanReload();
	void AttachWeaponToSocket(AUECBaseWeapon* Weapon, USceneComponent* SceneComponent, const FName& SocketName);
	void SpawnWeapons();
	void PlayAnimMontage(UAnimMontage* Animation);
	void InitAnimation();
	void OnEquipFinished(USkeletalMeshComponent* MeshComponent);
	void OnReloadFinished(USkeletalMeshComponent* MeshComponent);
	void OnEmptyClip(AUECBaseWeapon* AmmoEmptyWeapon);
	void ChangeClip();
};
