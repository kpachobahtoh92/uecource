// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "UECBaseCharacter.h"
#include "UECPlayerCharacter.generated.h"

class USpringArmComponent;
class UCameraComponent;
class USphereComponent;

UCLASS()
class UECOURCE_API AUECPlayerCharacter : public AUECBaseCharacter
{
	GENERATED_BODY()
public:
	// Sets default values for this character's properties
	AUECPlayerCharacter(const FObjectInitializer& ObjInit);

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	USpringArmComponent* SpringArmComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	USphereComponent* CameraCollisionComponent;

	virtual void OnDeath() override;
	void CheckCameraOverlap();
	UFUNCTION()
	void OnCameraCollisionComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex,
	                                            bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnCameraCollisionComponentEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	virtual void BeginPlay() override;

private:

	void MoveForward(float Amount);
	void MoveRight(float Amount);
	void StartRun();
	void EndRun();
};
