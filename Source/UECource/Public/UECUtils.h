#pragma once
#include "Player/UECPlayerState.h"


class UECUtils
{
public:
	template<typename T>
	static T* GetUECPlayerComponent(AActor* PlayerPawn)
	{
		if (!PlayerPawn)
		{
			return nullptr;
		}
		const auto Component = PlayerPawn->GetComponentByClass(T::StaticClass());
		return Cast<T>(Component);
	}

	bool static AreEnemies(AController* Controller1, AController* Controller2)
	{
		if (!Controller1 || !Controller2 || Controller1 == Controller2)
		{
			return false;
		}
		AUECPlayerState* PlayerState1 = Controller1->GetPlayerState<AUECPlayerState>();
		AUECPlayerState* PlayerState2 = Controller2->GetPlayerState<AUECPlayerState>();
		return PlayerState1 && PlayerState2 && PlayerState1->GetTeamID() != PlayerState2->GetTeamID();
	}

	static FText TextFromInt(int32 Number)
	{
		return FText::FromString(FString::FromInt(Number));
	}
};
