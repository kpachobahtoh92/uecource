// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "UECAnimNotify.h"
#include "UECEquipFinishedAnimNotify.generated.h"



/**
 * 
 */
UCLASS()
class UECOURCE_API UUECEquipFinishedAnimNotify : public UUECAnimNotify
{
	GENERATED_BODY()
};
