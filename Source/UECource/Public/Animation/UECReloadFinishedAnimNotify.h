// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "UECAnimNotify.h"
#include "UECReloadFinishedAnimNotify.generated.h"

/**
 * 
 */
UCLASS()
class UECOURCE_API UUECReloadFinishedAnimNotify : public UUECAnimNotify
{
	GENERATED_BODY()
	
};
