// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "UECBasePickup.h"
#include "UECAmmoPickup.generated.h"

class AUECBaseWeapon;

UCLASS()
class UECOURCE_API AUECAmmoPickup : public AUECBasePickup
{
	GENERATED_BODY()

public:
	AUECAmmoPickup();

protected:
	virtual void BeginPlay() override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup", meta = (ClampMin = "1.0", ClampMax = "10.0"))
	int32 ClipsAmount = 10;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
	TSubclassOf<AUECBaseWeapon> WeaponType;

public:
	virtual void Tick(float DeltaTime) override;

private:
	virtual bool GivePickupTo(APawn* PlayerPawn) override;
};
