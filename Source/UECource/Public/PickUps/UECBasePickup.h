// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UECBasePickup.generated.h"

class USphereComponent;

UCLASS()
class UECOURCE_API AUECBasePickup : public AActor
{
	GENERATED_BODY()

public:
	AUECBasePickup();

protected:
	virtual void BeginPlay() override;

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	
	UPROPERTY(VisibleAnywhere, Category = "Pickup")
	USphereComponent* CollisionComponent;

	UPROPERTY(VisibleAnywhere, Category = "Pickup")
	float RespawnTime = 5.f;

public:
	virtual void Tick(float DeltaTime) override;
	bool CouldBeTaken() const;

private:
	float RotationYaw = 0.f;
	virtual bool GivePickupTo(APawn* PlayerPawn);
	FTimerHandle RespawnTimerHandle;
	
	void PickUpWasTaken();
	void RespawnPickup();
	void GenerateRotationYaw();
};
