// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "UECBasePickup.h"
#include "UECHealthPickup.generated.h"

UCLASS()
class UECOURCE_API AUECHealthPickup : public AUECBasePickup
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AUECHealthPickup();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup", meta = (ClampMin = "1.0", ClampMax = "100.0"))
	int32 HealthAmount = 100;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	virtual bool GivePickupTo(APawn* PlayerPawn) override;
};
