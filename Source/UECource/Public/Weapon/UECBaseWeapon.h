// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UECCoreTypes.h"
#include "UECBaseWeapon.generated.h"

class UNiagaraSystem;
class UNiagaraComponent;

UCLASS()
class UECOURCE_API AUECBaseWeapon : public AActor
{
	GENERATED_BODY()

public:
	AUECBaseWeapon();

	FOnClipEmptySignature OnClipEmpty;

	virtual void StartFire();
	virtual void StopFire();
	
	void ChangeClip();
	bool CanReload() const;

	FWeaponUIData GetUIData () const {return UIData;}
	FAmmoData GetAmmoData () const {return CurrentAmmo;}
	bool TryToAddAmmo(int32 ClipsAmount);
	bool IsAmmoEmpty() const;

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Conponents")
	USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	FName MuzzleSocketName = "MuzzleSocket";
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float TraceMaxDistance = 1500.f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	FAmmoData DefaultAmmo{15, 10, false};

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	FWeaponUIData UIData;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	UNiagaraSystem* MuzzleFX;

	virtual void BeginPlay() override;
	virtual void MakeShot();
	virtual bool GetTraceData(FVector& TraceStart, FVector& TraceEnd);

	AController* GetController();
	bool GetPlayerViewPoint(FVector& ViewLocation, FRotator& ViewRotation);
	FVector GetMuzzleWorldLocation();
	
	void MakeHit(FHitResult& HitResult, FVector TraceStart, FVector TraceEnd);

	void DecreaseAmmo();
	
	bool IsClipEmpty() const;
	bool IsAmmoFull() const;
	void LogAmmo();

	UNiagaraComponent* SpawnMuzzleFX();

private:
	UPROPERTY()
	FAmmoData CurrentAmmo;
	
};
