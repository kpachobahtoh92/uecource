// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UECProjectile.generated.h"

class UUECWeaponFXComponent;
class UProjectileMovementComponent;
class USphereComponent;

UCLASS()
class UECOURCE_API AUECProjectile : public AActor
{
	GENERATED_BODY()

public:
	AUECProjectile();

	FORCEINLINE void SetShootDirection(const FVector& Direction) {ShootDirection = Direction;}

protected:
	UPROPERTY(VisibleAnywhere, Category = "Weapon")
	USphereComponent* CollisionComponent;
	
	UPROPERTY(VisibleAnywhere, Category = "Weapon")
	UProjectileMovementComponent* MovementComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float DamageRadius = 200.f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float DamageAmount = 50.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	bool bIsDoFullDamage = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float LifeSeconds = 5.f;
	
	UPROPERTY(VisibleAnywhere, Category = "FX")
	UUECWeaponFXComponent* WeaponFXComponent;

	UFUNCTION()
	void OnProjectileHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	virtual void BeginPlay() override;
	
private:
	FVector ShootDirection;

	AController* GetController() const;
	
};
