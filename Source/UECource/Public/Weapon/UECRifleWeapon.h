// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "UECBaseWeapon.h"
#include "UECRifleWeapon.generated.h"

class UUECWeaponFXComponent;
class UNiagaraComponent;
class UNiagaraSystem;

UCLASS()
class UECOURCE_API AUECRifleWeapon : public AUECBaseWeapon
{
	GENERATED_BODY()

public:
	AUECRifleWeapon();
	virtual void BeginPlay() override;
	virtual void StartFire() override;
	virtual void StopFire() override;
	
protected:
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float TimeBetweenShots = 0.1f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float BulletSpread = 1.5f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float DamageAmount = 10.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	UNiagaraSystem* TraceFX;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	FString TraceTargetName = "TraceTarget";
	
	UPROPERTY(VisibleAnywhere, Category = "VFX")
	UUECWeaponFXComponent* WeaponFXComponent;
	
	virtual void MakeShot() override;
	virtual bool GetTraceData(FVector& TraceStart, FVector& TraceEnd) override;

	void MakeDamage(const FHitResult& HitResult);
private:
	FTimerHandle ShotTimerHandle;

	UPROPERTY()
	UNiagaraComponent* MuzzleFXComponent;
	void InitMuzzleFX();
	void SetMuzzleFXVisibility(bool Visibility);
	void SpawnTraceFX(const FVector& TraceStart, const FVector& TraceEnd);

};
