// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "UECBaseWeapon.h"
#include "UECLauncherWeapon.generated.h"

class AUECProjectile;

UCLASS()
class UECOURCE_API AUECLauncherWeapon : public AUECBaseWeapon
{
	GENERATED_BODY()
	AUECLauncherWeapon();
public:
	virtual void StartFire() override;
protected:
	virtual void MakeShot() override;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	TSubclassOf<AUECProjectile> ProjectileClass;

};
