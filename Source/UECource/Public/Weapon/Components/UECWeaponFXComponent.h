// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "UECCoreTypes.h"
#include "UECWeaponFXComponent.generated.h"

class UPhysicalMaterial;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class UECOURCE_API UUECWeaponFXComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UUECWeaponFXComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
	                           FActorComponentTickFunction* ThisTickFunction) override;

	void PlayImpactFX (const FHitResult& Hit);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FX")
	FImpactData DefaultImpactData;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FX")
	TMap<UPhysicalMaterial*, FImpactData> ImpactDataMap;
};
