// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UECEntryGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UECOURCE_API AUECEntryGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	AUECEntryGameModeBase();
};
