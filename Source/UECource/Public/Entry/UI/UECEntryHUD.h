// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "UECEntryHUD.generated.h"

/**
 * 
 */
UCLASS()
class UECOURCE_API AUECEntryHUD : public AHUD
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> EntryUserWidgetClass;

	virtual void BeginPlay() override;
};
