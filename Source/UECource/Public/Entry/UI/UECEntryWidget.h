// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "UECCoreTypes.h"
#include "Blueprint/UserWidget.h"
#include "UECEntryWidget.generated.h"

class UUECGameInstance;
class UUECLevelItemWidget;
class UHorizontalBox;
class UButton;
/**
 * 
 */
UCLASS()
class UECOURCE_API UUECEntryWidget : public UUserWidget
{
	GENERATED_BODY()
protected:
	UPROPERTY(meta = (BindWidget))
	UButton* StartGameButton;
	
	UPROPERTY(meta = (BindWidget))
	UHorizontalBox* LevelItemsBox;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> LevelItemWidgetClass;

	virtual void NativeOnInitialized() override;

private:
	UPROPERTY()
	TArray<UUECLevelItemWidget*> LevelItemWidgets;

	void InitLevelItems();
	void OnLevelSelected(const FLevelData& Data);
	UUECGameInstance* GetUECGameInstance() const;
	
	UFUNCTION()
	void OnStartGame();
};
