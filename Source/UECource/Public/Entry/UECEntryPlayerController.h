// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "UECEntryPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class UECOURCE_API AUECEntryPlayerController : public APlayerController
{
	GENERATED_BODY()

	virtual void BeginPlay() override;
};
