// Unreal Engine cource project.

#pragma once

#include "CoreMinimal.h"
#include "UECCoreTypes.h"
#include "GameFramework/GameModeBase.h"
#include "UECGameModeBase.generated.h"

class AAIController;

UCLASS()
class UECOURCE_API AUECGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	AUECGameModeBase();

	FOnMatchStateChangedSignature OnMatchStateChanged;

	virtual void StartPlay() override;

	virtual UClass* GetDefaultPawnClassForController_Implementation(AController* InController) override;

	void Killed(AController* KillerController,AController* VictimController);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "GameMode|RoundInfo")
	FORCEINLINE int32 GetCurrentRound() const{return CurrentRound;}
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "GameMode|RoundInfo")
	FORCEINLINE int32 GetTotalRounds() const{return GameData.RoundNum;}
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "GameMode|RoundInfo")
	FORCEINLINE int32 GetRoundCountDown() const{return RoundCountDown;}

	void RespawnRequest(AController* Controller);

	virtual bool SetPause(APlayerController* PC, FCanUnpause CanUnpauseDelegate) override;
	virtual bool ClearPause() override;

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Game")
	TSubclassOf<AAIController> AIControllerClass;

	UPROPERTY(EditDefaultsOnly, Category = "Game")
	TSubclassOf<APawn> AIPawnClass;

	UPROPERTY(EditDefaultsOnly, Category = "Game")
	FGameData GameData;

private:
	EUECMatchState MatchState = MS_WaitingToStart;
	int32 CurrentRound = 1;
	int32 RoundCountDown = 0;

private:
	FTimerHandle GameRoundTimerHandle;
	
	void SpawnBots();
	void StartRound();
	void GameTimerUpdate();
	
	void ResetPlayers();
	void ResetOnePlayer(AController* Controller);

	void CreateTeamInfo();
	FLinearColor DetermineColorByTeamID(int32 TeamID) const;
	void SetPlayerColor(AController* Controller);

	void LogPlayerInfo();

	void StartRespawn(AController* Controller);

	void GameOver();

	void SetMatchState(EUECMatchState State);
};
