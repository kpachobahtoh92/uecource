// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "UECIceDamageType.generated.h"

/**
 * 
 */
UCLASS()
class UECOURCE_API UUECIceDamageType : public UDamageType
{
	GENERATED_BODY()
};
