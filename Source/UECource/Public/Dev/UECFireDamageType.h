// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "UECFireDamageType.generated.h"

/**
 * 
 */
UCLASS()
class UECOURCE_API UUECFireDamageType : public UDamageType
{
	GENERATED_BODY()
};
