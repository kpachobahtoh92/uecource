// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "UECCoreTypes.h"
#include "GameFramework/HUD.h"
#include "UECGameHUD.generated.h"

/**
 * 
 */
UCLASS()
class UECOURCE_API AUECGameHUD : public AHUD
{
	GENERATED_BODY()

public:

	virtual void DrawHUD() override;
	virtual void BeginPlay() override;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> PlayerHUDWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> PauseWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> GameOverWidgetClass;

private:
	UPROPERTY()
	TMap<TEnumAsByte<EUECMatchState>, UUserWidget*> GameWidgets;
	
	UPROPERTY()
	UUserWidget* CurrentWidget = nullptr;
	
	void DrawCrosshair();
	void OnMatchStateChange(EUECMatchState State);
};
