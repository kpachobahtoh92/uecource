// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "UECButtonBase.h"
#include "UECButtonExit.generated.h"

/**
 * 
 */
UCLASS()
class UECOURCE_API UUECButtonExit : public UUECButtonBase
{
	GENERATED_BODY()

public:
	virtual void Clicked() override;
};
