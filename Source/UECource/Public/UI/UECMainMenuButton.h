// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "UECButtonBase.h"
#include "UECMainMenuButton.generated.h"

/**
 * 
 */
UCLASS()
class UECOURCE_API UUECMainMenuButton : public UUECButtonBase
{
	GENERATED_BODY()

public:
	virtual void Clicked() override;
};
