// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UECPauseWidget.generated.h"

class UButton;
/**
 * 
 */
UCLASS()
class UECOURCE_API UUECPauseWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	virtual void NativeOnInitialized() override;
protected:
	UPROPERTY(meta = (BindWidget))
	UButton* ClearPauseButton;

private:
	UFUNCTION()
	void OnClearPause();
};
