// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Border.h"
#include "Components/TextBlock.h"
#include "UECPlayerStatRowWidget.generated.h"

class UImage;

UCLASS()
class UECOURCE_API UUECPlayerStatRowWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	FORCEINLINE void SetPlayerText(const FText& Text){PlayerNameTextBlock->SetText(Text);}
	FORCEINLINE void SetKillsText(const FText& Text){KillsTextBlock->SetText(Text);}
	FORCEINLINE void SetDeathsText(const FText& Text){DeathsTextBlock->SetText(Text);}
	FORCEINLINE void SetTeamText(const FText& Text){TeamTextBlock->SetText(Text);}
	FORCEINLINE void SetPlayerIndicatorVisibility(ESlateVisibility NewVisibility){PlayerIndicatorBorder->SetVisibility(NewVisibility);}
	void SetTeamColor(const FLinearColor& TeamColor);
	
protected:

	UPROPERTY(meta = (BindWidget))
	UTextBlock* PlayerNameTextBlock;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* KillsTextBlock;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* DeathsTextBlock;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* TeamTextBlock;

	UPROPERTY(meta = (BindWidget))
	UBorder* PlayerIndicatorBorder;

	UPROPERTY(meta = (BindWidget))
	UImage* TeamImage;
};
