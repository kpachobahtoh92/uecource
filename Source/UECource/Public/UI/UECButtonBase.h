// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UECButtonBase.generated.h"

class UTextBlock;
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnButtonBaseClickedEvent);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnButtonBasePressedEvent);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnButtonBaseReleasedEvent);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnButtonBaseHoverEvent);

class UButton;
/**
 * 
 */
UCLASS()
class UECOURCE_API UUECButtonBase : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION()
	virtual void Clicked();
	UFUNCTION()
	virtual void Pressed();
	UFUNCTION()
	virtual void Hovered();
	UFUNCTION()
	virtual void Unhovered();
	UFUNCTION()
	virtual void Released();
	
	virtual void NativeOnInitialized() override;

	virtual void NativePreConstruct() override;

	UPROPERTY(BlueprintReadOnly ,meta = (BindWidget))
	UButton* BaseButton;

	UPROPERTY(BlueprintReadOnly ,meta = (BindWidget))
	UTextBlock* ButtonTextBlock;

	UPROPERTY(EditAnywhere)
	FText ButtonText = FText::FromString("Button");
	
	UPROPERTY(BlueprintAssignable, Category="Event")
	FOnButtonBaseClickedEvent OnButtonBaseClicked;
	
	UPROPERTY(BlueprintAssignable, Category="Event")
	FOnButtonBasePressedEvent OnButtonBasePressed;
	
	UPROPERTY(BlueprintAssignable, Category="Event")
	FOnButtonBaseReleasedEvent OnButtonBaseReleased;
	
	UPROPERTY(BlueprintAssignable, Category="Event")
	FOnButtonBaseHoverEvent OnButtonBaseHovered;

	UPROPERTY(BlueprintAssignable, Category="Event")
	FOnButtonBaseHoverEvent OnButtonBaseUnHovered;
};
