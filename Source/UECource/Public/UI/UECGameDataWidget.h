// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UECGameDataWidget.generated.h"

/**
 * 
 */
UCLASS()
class UECOURCE_API UUECGameDataWidget : public UUserWidget
{
	GENERATED_BODY()
};
