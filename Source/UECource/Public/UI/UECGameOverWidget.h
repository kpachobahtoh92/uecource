// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "UECCoreTypes.h"
#include "Blueprint/UserWidget.h"
#include "UECGameOverWidget.generated.h"

class UButton;
class UUECPlayerStatRowWidget;
class UVerticalBox;
/**
 * 
 */
UCLASS()
class UECOURCE_API UUECGameOverWidget : public UUserWidget
{
	GENERATED_BODY()

protected:

	UPROPERTY(meta = (BindWidget))
	UVerticalBox* PlayerStatBox;

	UPROPERTY(meta = (BindWidget))
	UButton* ResetLevelButton;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUECPlayerStatRowWidget> PlayerStatRowWidgetClass;

	virtual void NativeOnInitialized() override;

private:
	void UpdatePlayerStat();
	void OnMatchStateChange(EUECMatchState MatchState);

	UFUNCTION()
	void OnResetLevel();
};
