// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UECSpectatorWidget.generated.h"

/**
 * 
 */
UCLASS()
class UECOURCE_API UUECSpectatorWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "UI")
	bool GetRespawnTime(int32& CountDownTime) const;
};
