// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UECCoreTypes.h"
#include "UECPlayerHUDWidget.generated.h"

class UProgressBar;
/**
 * 
 */
UCLASS()
class UECOURCE_API UUECPlayerHUDWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void OnNewPawn(APawn* Pawn);
	
	UFUNCTION(BlueprintCallable, Category = "UI")
	float GetHealthPercent() const;

	UFUNCTION(BlueprintCallable, Category = "UI")
	bool GetCurrentWeaponUIData(FWeaponUIData& UIData) const;

	UFUNCTION(BlueprintCallable, Category = "UI")
	bool GetCurrentWeaponAmmoData(FAmmoData& AmmoData) const;

	UFUNCTION(BlueprintCallable, Category = "UI")
	bool IsPlayerAlive() const;
	
	UFUNCTION(BlueprintCallable, Category = "UI")
	bool IsPlayerSpectatig() const;

	UFUNCTION(BlueprintImplementableEvent, Category = "UI")
	void OnTakeAnyDamage();

	UFUNCTION(BlueprintCallable, Category = "UI")
	int32 GetKillsNum() const;
protected:
	
	virtual void NativeOnInitialized() override;
	
	UPROPERTY(meta = (BindWidget))
	UProgressBar* HealthProgressBar;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	float PercentColorThreshold = 0.3f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	FLinearColor GoodColor = FLinearColor::White;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	FLinearColor BadColor = FLinearColor::Red;

private:
	void OnHealthChanged (float Health, float HealthDelta);
	void UpdateHealthBar();
};
