// Unreal Engine course project

#pragma once

#include "CoreMinimal.h"
#include "UECCoreTypes.h"
#include "Engine/GameInstance.h"
#include "UECGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class UECOURCE_API UUECGameInstance : public UGameInstance
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Game")
	FName EntryLevelName = NAME_None;

	UPROPERTY(EditDefaultsOnly, Category = "Game", meta = (ToolTip = "Level name must be unique!"))
	TArray<FLevelData> LevelsData;

private:
	FLevelData StartupLevel;

public:
	FORCEINLINE FLevelData GetStartupLevel() const { return StartupLevel; }
	FORCEINLINE void SetStartupLevel(const FLevelData& Data) { StartupLevel = Data; }

	FORCEINLINE TArray<FLevelData> GetLevelsData() const {return LevelsData; }
	FORCEINLINE FName GetEntryLevelName() const { return EntryLevelName; }
};
