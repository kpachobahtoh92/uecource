// Unreal Engine cource project.

using UnrealBuildTool;
using System.Collections.Generic;

public class UECourceTarget : TargetRules
{
	public UECourceTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V4;

		ExtraModuleNames.AddRange( new string[] { "UECource" } );
	}
}
