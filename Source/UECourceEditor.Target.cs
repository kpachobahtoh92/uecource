// Unreal Engine cource project.

using UnrealBuildTool;
using System.Collections.Generic;

public class UECourceEditorTarget : TargetRules
{
	public UECourceEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V4;

		ExtraModuleNames.AddRange( new string[] { "UECource" } );
	}
}
